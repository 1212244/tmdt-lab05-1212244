﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212244.Models
{
    public class UsersRepository : IUsersRepository
    {
        private List<Users> user = new List<Users>();
        private int _nextId = 1;

        public UsersRepository()
        {
            Add(new Users { Id = 1, Username = "1212244", Password = "*****" });
            Add(new Users { Id = 2, Username = "1212245", Password = "******" });
            Add(new Users { Id = 3, Username = "1212246", Password = "*******" });
            Add(new Users { Id = 4, Username = "1212247", Password = "********" });
            Add(new Users { Id = 5, Username = "1212248", Password = "*********" });
        }

        //Phương thức xuất tất cả Users
        public IEnumerable<Users> GetAll()
        {
            return user;
        }

        //Phương thức get 1 user theo username
        public Users Get(string username)
        {
            return user.Find(p => p.Username == username);
        }

        //Phương thức thêm 1 user vào List
        public Users Add(Users item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            user.Add(item);
            return item;
        }

        //Phương thức xóa 1 user từ List
        public void Remove(string username)
        {
            user.RemoveAll(p => p.Username == username);
        }

        //Phương thức Update thông tin user
        public bool Update(Users item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = user.FindIndex(p => p.Username == item.Username);
            if (index == -1)
            {
                return false;
            }
            user.RemoveAt(index);
            user.Add(item);
            return true;
        }
    }
}