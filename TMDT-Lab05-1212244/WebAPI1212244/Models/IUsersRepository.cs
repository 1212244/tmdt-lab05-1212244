﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212244.Models
{
    interface IUsersRepository
    {
        IEnumerable<Users> GetAll();
        Users Get(string username);
        Users Add(Users item);
        void Remove(string username);
        bool Update(Users item);
    }
}
