﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI1212244.Models;

namespace WebAPI1212244.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        static readonly IUsersRepository repo = new UsersRepository();

        //Hàm xuất tất cả Users
        [HttpGet]
        [Route("api/2.0/users")]
        public IEnumerable<Users> GetAll1212244()
        {
            return repo.GetAll();
        }

        //Hàm lấy 1 User theo username
        [HttpGet]
        [Route("api/2.0/users/{username}")]
        public Users Get1212244(string username)
        {
            Users item = repo.Get(username);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        //Hàm thêm 1 User
        [HttpPost]
        [Route("api/2.0/users")]
        public Users Post1212244([FromBody]Users item)
        {
            item = repo.Add(item);
            return item;
        }

        //Hàm xóa 1 user
        [HttpDelete]
        public void Delete1212244(string username)
        {
            Users item = repo.Get(username);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repo.Remove(username);
        }

        //Hàm cập nhật thông tin user
        [HttpPut]
        [Route("api/2.0/users/{username}")]
        public void Put1212244(string username, [FromBody]Users user)
        {
            user.Username = username;
            if (!repo.Update(user))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
