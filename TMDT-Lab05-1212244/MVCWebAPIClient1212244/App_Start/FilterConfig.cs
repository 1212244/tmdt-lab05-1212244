﻿using System.Web;
using System.Web.Mvc;

namespace MVCWebAPIClient1212244
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
